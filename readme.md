# Alto Gamer YouTube jQuery Plugin
##### A jQuery plugin to display videos from YouTube using a query string.

## What is this plugin?
Alto Gamer YouTube jQuery Plugin displays videos from YouTube using a query string.
It also displays thumbnails for the videos, and allows the user to click on
a thumbnail to watch the video in a larger area.

This plugin was first coded to be used in http://www.altogamer.com - a friendly
videogames website :)

## License
Alto Gamer YouTube jQuery Plugin is distributed under the Mozilla Public License, version 2.0.
The LICENSE file contains more information about the licesing of this product.
You can read more about the MPL at Mozilla Public License FAQ.